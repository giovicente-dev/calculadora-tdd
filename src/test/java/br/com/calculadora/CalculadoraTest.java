package br.com.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculadoraTest {

    private Calculadora calculadora;

    @BeforeEach
    public void setUp() {
        calculadora = new Calculadora();
    }

    @Test
    public void testaASomaDeDoisNumerosInteiros() {
        int resultado = calculadora.soma(1, 2);

        Assertions.assertEquals(3, resultado);
    }

    @Test
    public void testaASomaDeDoisNumerosFlutuantes() {
        double resultado = calculadora.soma(2.3, 3.4);

        Assertions.assertEquals(5.7, resultado);
    }

    @Test
    public void testaADivisaoDeDoisNumerosInteirosPositivos() {
        int resultado = calculadora.dividir(8, 4);

        Assertions.assertEquals(2, resultado);
    }

    @Test
    public void testaADivisaoDeDoisNumerosInteirosNegativos() {
        int resultado = calculadora.dividir(-12, -4);

        Assertions.assertEquals(3, resultado);
    }

    @Test
    public void testaAdivisaoDeDoisNumerosFlutuantes() {
        double resultado = calculadora.dividir(8.5, 2.5);

        Assertions.assertEquals(3.4, resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosInteiros() {
        int resultado = calculadora.multiplicar(4, 5);

        Assertions.assertEquals(20, resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosNegativos() {
        int resultado = calculadora.multiplicar(-20, -10);

        Assertions.assertEquals(200, resultado);
    }

    @Test
    public void testaAMultipliacaoDeDoisNumerosFlutuantes() {
        double resultado = calculadora.multiplicar(1.5, 2.5);

        Assertions.assertEquals(3.75, resultado);
    }

}

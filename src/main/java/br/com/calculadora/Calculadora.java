package br.com.calculadora;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Calculadora {

    public Calculadora() { }

    public int soma(int primeiroNumero, int segundoNumero) {
        int resultado = primeiroNumero + segundoNumero;
        return resultado;
    }

    public double soma(double primeiroNumero, double segundoNumero) {
        Double resultado = primeiroNumero + segundoNumero;
        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(3, RoundingMode.HALF_EVEN);

        return bigDecimal.doubleValue();
    }

    public int dividir(int primeiroNumero, int segundoNumero) {
        int resultado = primeiroNumero / segundoNumero;
        return resultado;
    }

    public double dividir(double primeiroNumero, double segundoNumero) {
        Double resultado = primeiroNumero / segundoNumero;
        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(3, RoundingMode.HALF_EVEN);

        return bigDecimal.doubleValue();
    }

    public int multiplicar(int primieroNumero, int segundoNumero) {
        int resultado = primieroNumero * segundoNumero;
        return resultado;
    }

    public double multiplicar(double primeiroNumero, double segundoNumero) {
        Double resultado = primeiroNumero * segundoNumero;
        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(3, RoundingMode.HALF_EVEN);

        return resultado;
    }

}
